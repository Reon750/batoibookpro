#!/bin/sh

# Run Composer install
composer install --no-dev --prefer-dist --no-interaction --optimize-autoloader

# Set permissions for Laravel
chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache

php artisan migrate

# Start PHP-FPM
php-fpm
